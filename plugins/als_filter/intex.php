<?php

/**
* Plugin Name: Joornal User Centric
* Plugin URI: https://wordpress.org/
* Version: 1.0
* Author: Arthur KATZ
**/

/**
 * Includes the ability to register on the site. Standard subscriber role.
 */
register_activation_hook( __FILE__, 'filter_activation' );
function filter_activation(){
  update_option( 'users_can_register', 1);
  update_option( 'default_role', 'subscriber');
}

/**
 * When you turn off the plugin removes the possibility of registration
 */
register_deactivation_hook(__FILE__, 'filter_deactivation');
function filter_deactivation(){
  update_option( 'users_can_register', 0);
}

/**
 * add scripts  in future 
 */

// add_action( 'wp_enqueue_scripts', 'add_script_and_style');

// function add_script_and_style(){
//   if(is_home()){
//     wp_enqueue_script( 'main_js_plugin', plugins_url() . '/als_filter/js/main.js', array('bootstrap_4_js'), '1.0.0', true);
//     wp_enqueue_script( 'bootstrap_4_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true);
//     wp_enqueue_style('main_style_plugin', "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css");
//   }
  
// }

/**
 * add custom fields in form register
 */

add_action( 'register_form', 'filter_register_user' );
function filter_register_user() {

    $atr_user = ( ! empty( $_POST['atr_user'] ) ) ? trim( $_POST['atr_user'] ) : '';

    $html = '<p>
      <input type="radio" name="atr_user" value="pregnant"  checked="checked"> Pregnant<br>
      <input type="radio" name="atr_user" value="mom"> Mom<br>
      <input type="radio" name="atr_user" value="dad"> Dad<br>  
    </p><br>';
    echo $html;
}

/**
 * Updating user metadata
 */

add_action( 'user_register', 'filter_add_data_user_register' );
function filter_add_data_user_register( $user_id ) {
    if ( ! empty( $_POST['atr_user'] ) ) {
        update_user_meta( $user_id, 'atr_user', trim( $_POST['atr_user'] ) );
    }
}


/**
 * Filtering the issuance of posts depending on the user
 */
add_action("init", 'start');

function start(){


  if(is_user_logged_in()){
    $userID = wp_get_current_user();
    $userStatus = get_user_meta( $userID->ID, 'atr_user', true );
    
    switch ($userStatus){
      case 'pregnant':
        query_posts( 'cat=34');
        break;

      case 'mom':
        query_posts( 'cat=35');
        break;

      case 'dad':
        query_posts( 'cat=36');
        break;
    }
  }
}


?>